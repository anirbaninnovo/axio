<?php get_header(); ?>

    <section class="row breadcrumbRow">
        <div class="container">
            <div class="row inner m0">
                <?php echo medical_pro_breadcrumb(); ?>
            </div>
        </div>
    </section>

    <section class="row content_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 blog_list">
                    <!--Loop Start-->
                    <?php

                    if(have_posts()) :
                        while(have_posts())
                        {
                            the_post();
                            $archive_year  = get_the_time('Y');
                            $archive_month = get_the_time('m');
                            $archive_day   = get_the_time('d');
    ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class("row m0 blog blog2"); ?>>
                                <div class="media-body">
                                    <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                                    <div class="row m0 meta"><?php esc_html_e('By ', 'medical-pro'); ?> : <?php the_author_posts_link(); ?> <?php esc_html_e('on', 'medical-pro'); ?> : <a href="<?php echo esc_url(get_day_link( $archive_year, $archive_month, $archive_day)); ?>"><?php the_time(get_option('date_format')); ?></a> <?php esc_html_e('comments', 'medical-pro'); ?> : (<?php comments_popup_link(esc_html__('0', 'medical-pro'), esc_html__('1', 'medical-pro'), esc_html__('%', 'medical-pro')); ?>)</div>
                                    <p><?php the_excerpt(); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="view_all"><?php esc_html_e('read more', 'medical-pro'); ?></a>
                                </div>
                            </div>
                            <?php
                        }
                    else :
                        get_template_part( 'content', 'none' );
                    endif;
                    global $wp_query;
                    medical_pro_pagination($wp_query);
                    ?>
                    <!--single blog-->
                    <!--Loop End-->
                </div>
                <div class="col-sm-12 col-md-4 sidebar">
                    <?php
                    if ( is_active_sidebar( 'blog-post' ) ){
                        dynamic_sidebar( 'blog-post' );
                    }
                    ?>

                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>