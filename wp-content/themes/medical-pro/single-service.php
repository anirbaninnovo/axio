<?php get_header(); ?>

    <section class="row breadcrumbRow">
        <div class="container">
            <div class="row inner m0">
                <?php echo medical_pro_breadcrumb(); ?>
            </div>
        </div>
    </section>

<?php
// Start the loop.
while ( have_posts() ) : the_post();
 $serivce_icon = get_post_meta(get_the_ID(), 'serivce_icon_style2', true);
    ?>
    <section class="row service_details">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 serviceDetailsSection">
                    <h2 class="post_title"><span class="post_icon"><img src="<?php echo esc_url($serivce_icon); ?>"/></span><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
                <?php get_sidebar('service') ?>
            </div>
        </div>
    </section>
<?php endwhile; ?>
<?php get_footer(); ?>