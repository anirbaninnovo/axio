/*
    Main Jquery File
*/

(function($) {
    "use strict";

    jQuery.fn.shake = function(intShakes, intDistance, intDuration) {
        this.each(function() {
            $(this).css("position","relative");
            for (var x=1; x<=intShakes; x++) {
                $(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
                    .animate({left:intDistance}, ((intDuration/intShakes)/2))
                    .animate({left:0}, (((intDuration/intShakes)/4)));
            }
        });
        return this;
    };


$(document).ready(function(){
$(".joinaxiobio").click(function(){

var positiondata = $(this).html();
$("#checkposition input").val(positiondata);

});

$(".joinaxiobioapplybtn").click(function(){

var positiondata = $(this).attr('title');
$("#checkposition input").val(positiondata);
console.log(positiondata);

});
});
    

    $(document).ready(function(){       
        
        /*----------------------------------------------------*/
        /*  Slider Preloader
        /*----------------------------------------------------*/
        $(".preloader").delay(700).fadeOut();
        
        /*----------------------------------------------------*/
        /*  Appointment Date
        /*----------------------------------------------------*/
        $('input[name="date"]').datepicker();
    
        /*----------------------------------------------------*/
        /*  Go Top
        /*----------------------------------------------------*/
        $('a[href="#appointment"]').click(function () {
            $('html, body').animate({ scrollTop: 350 }, 800);
            return false
        });  
        
        /*----------------------------------------------------*/
        /*  Appointment Date
        /*----------------------------------------------------*/
        $('.appointment_home_form input').blur(function () {
            if ($(this).val()) {
                $(this).addClass('notEmpty')
            }
            else{
                $(this).removeClass('notEmpty')
            }
        });
        
        /*----------------------------------------------------*/
        /*  Time Table Filter
        /*----------------------------------------------------*/
        var tableCell = $('.cell');
        $('.timeTableFilters li').on('click', function () {
            $('.active').removeClass('active');
            $(this).addClass('active');
            
            var filter_val = $(this).attr('data-filter');
            
            tableCell.addClass('bgf');            
            if(filter_val == 'all'){
                tableCell.removeClass('bgf')
            }
            else{
                tableCell.addClass('bgf');
                $('.timeTable td.'+ filter_val).removeClass('bgf')
            }            
        });
		

	$('#service_tab li').on('click', function () {
            $('#service_tab .active').removeClass('active');
	       var id = $(this).attr('id');	
             $("#"+id+ " a span").addClass('active');
        });

        $('.appointment_form').submit(function(e){
            $('.appointment_form .loading').show();

            $('.appointment_form .error').removeClass('error');
            var submit_btn = $(this).find('input[type=submit]');
            var old_val = submit_btn.val();
            submit_btn.attr('disabled', 'disabled');
            submit_btn.val('Submitting...');
			
			

            e.preventDefault();
            $('.appointment_form .alert').removeClass('alert-danger alert-success').html('').slideUp();
            $.post(MyAjax.ajaxurl, $(this).serialize()+'&action=book_appointment', function(data)
			{
				
                $('.appointment_form .loading').hide();
                submit_btn.removeAttr('disabled');
				
                submit_btn.val(old_val);
                if(data.success)
                {
                    $('.appointment_form .alert').addClass('alert-success').html(data.success).slideDown();
                } else if(data.error)
                {
                    $('.appointment_form .alert').addClass('alert-danger').html(data.error).slideDown();

                    $.each(data.errors, function(index, value)
					{
						
                        $('.appointment_form').find('label[for='+index+']').addClass('error');
						$('.appointment_form').find('div[for='+index+']').addClass('error');
                    });

                    $( "#appointmefnt_form_pop .modal-dialog" ).shake(3, 20, 300);
                }
            }, 'json');
        });

        $('form[name=newsletterForm]').submit(function(e){
            $('footer .msg').removeClass('error success').html('');
            $('form[name=newsletterForm] input').removeClass('error');
            $('form[name=newsletterForm] .loading').show();
            var submit_btn = $(this).find('input[type=submit]');
            submit_btn.attr('disabled', 'disabled');
            var old_val = submit_btn.val();
            submit_btn.val('Submitting...');

            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function(data){
                submit_btn.removeAttr('disabled');
                $('form[name=newsletterForm] .loading').hide();
                submit_btn.val(old_val);
                if(data.success)
                {
                    $('footer .msg').addClass('success').html(data.success);
                    $('input[type=text]').val('');
                } else if(data.error)
                {
                    $('footer .msg').addClass('error').html(data.error);
                    $.each(data.errors, function(index, value){
                        $('form[name=newsletterForm]').find('input[name='+index+']').addClass('error');
                    });
                    $('form[name=newsletterForm]').shake(3, 20, 300);
                }
            }, 'json');
        });

        $.each($('.vc_wp_custommenu'), function(){
            if($(this).width() < 400)
            {
                $(this).find('.menu li').css({'width':'49%'});
            }
        })

        
    });
    
    
        
    /*----------------------------------------------------*/
    /*  Testimonial Slider
    /*----------------------------------------------------*/    
    $('.testimonial_slider').flexslider({
        animation: "fade",
        directionNav: false
    });
        
	$('.gallery-slider').flexslider({
		animation: "slide",
		controlNav: false,
		directionNav: true,
		pauseOnHover: true,
		pauseOnAction: false,
		smoothHeight: true,
		start: function (slider) {
			slider.removeClass('loading');
		}
	});
	
	  if (jQuery().swipebox) {
        // Initialize the Lightbox automatically for any links to images with extensions .jpg, .jpeg, .png or .gif
        $("a[href$='.jpg'], a[href$='.png'], a[href$='.jpeg'], a[href$='.gif']").swipebox();
    }
	
    /*----------------------------------------------------*/
    /*  Aflix
    /*----------------------------------------------------*/
    $(".navbar2,.navbar3").affix({
        offset: {
            top: $('.top_bar').height()
        }
    });
    
})(jQuery)

