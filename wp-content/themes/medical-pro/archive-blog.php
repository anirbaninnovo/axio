<?php get_header(); ?>

    <section class="row breadcrumbRow">
        <div class="container">
            <div class="row inner m0">
                <ul class="breadcrumb">
                    <li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
                    <li><?php the_archive_title(); ?></li>
                </ul>
            </div>
        </div>
    </section>



<section class="nblog">
    <div class="container">
       <div class="row">
          
           <div class="col-sm-8">
           
            <?php
            
            $args = array( 'post_type' => 'blog' );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
            ?>
              <div class="row">
                <div class="col-sm-12">
                    
                        <?php echo the_post_thumbnail(); ?>
                        <h3><?php the_title(); ?></h3>
                        <?php the_excerpt(); ?>
                       
                
                </div><!-- col-->
               </div>  
            <?php endwhile; ?>
           </div>
           
           <div class="col-sm-4">
               <?php
                    if ( is_active_sidebar( 'blog-post' ) ){
                        dynamic_sidebar( 'blog-post' );
                    }
                    ?>
           </div>
           
        </div><!-- row -->
        
    </div>
</section>


<?php get_footer(); ?>