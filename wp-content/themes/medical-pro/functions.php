<?php

if ( ! isset( $content_width ) ) $content_width = 900;

function medical_pro_setup()
{

    // Make theme available for translation, translations can be filed in the /languages/ directory
    load_theme_textdomain('medical-pro', get_template_directory() . '/languages');

    // This theme uses post format support.
    add_theme_support('post-formats', array('gallery', 'link', 'image', 'quote', 'video', 'audio'));

    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
    add_theme_support('title-tag');

    // This theme uses post thumbnails
    add_theme_support('post-thumbnails');

    // Add default posts and comments RSS feed links to head
    add_theme_support('automatic-feed-links');

    // This theme uses menus
    add_theme_support('menus');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
        'top' => esc_html__('Top Navigation', 'medical-pro'),
        'footer' => esc_html__('Footer Navigation', 'medical-pro'),
    ));

    /* Add Post Thumbnails Support and Related Image Sizes */	
    add_theme_support('post-thumbnails');
	
	add_image_size('medical-pro-thumb-small', 42, 42, true);
	add_image_size('medical-pro-doctor-thumb', 262, 262, true);
    add_image_size('medical-pro-thumb-large', 495, 601, true);
	add_image_size('medical-pro-post-thumb', 185, 137, true);
	add_image_size('medical-pro-department-thumb', 569, 459, true);
	add_image_size('medical-pro-service-thumb-large', 555, 363, true);
	add_image_size('medical-pro-post-thumb-medium', 262, 202, true);
	add_image_size('medical-pro-post-thumb-widget', 143, 122, true);

}
add_action('after_setup_theme', 'medical_pro_setup');


if(!function_exists('medical_pro_add_editor_styles'))
{
    function medical_pro_add_editor_styles() {
        add_editor_style(get_stylesheet_uri());
        add_editor_style(get_template_directory_uri() . '/css/bootstrap.min.css');
        //add_editor_style(get_template_directory_uri() . '/css/bootstrap-theme.min.css');
        add_editor_style(get_template_directory_uri() . '/css/font-awesome.min.css');
        add_editor_style(get_template_directory_uri() . '/css/default/style.css');
    }
}
add_action('admin_init', 'medical_pro_add_editor_styles');


/*-----------------------------------------------------------------------------------*/
/*	Theme required plugin list
/*-----------------------------------------------------------------------------------*/
require_once ( get_template_directory() .'/include/plugins/class-tgm-plugin-activation.php');
require_once ( get_template_directory() .'/include/plugins/plugin_list.php');


/*-----------------------------------------------------------------------------------*/
/*	Theme custom function
/*-----------------------------------------------------------------------------------*/
require_once ( get_template_directory() .'/include/theme-functions.php');


/*-----------------------------------------------------------------------------------*/
/*	Theme option configuration
/*-----------------------------------------------------------------------------------*/
require_once ( get_template_directory() .'/include/medicalpro-config.php');


/*-----------------------------------------------------------------------------------*/
/*	Bootstrap WP Menu Walker
/*-----------------------------------------------------------------------------------*/
require_once ( get_template_directory() .'/include/wp_bootstrap_navwalker.php');


/*-----------------------------------------------------------------------------------*/
/*	AJAX Handler
/*-----------------------------------------------------------------------------------*/
require_once ( get_template_directory() .'/include/ajax.php');


/*-----------------------------------------------------------------------------------*/
/*	Register Widgets
/*-----------------------------------------------------------------------------------*/
require_once ( get_template_directory() .'/include/widget-services.php');
require_once ( get_template_directory() .'/include/widget-recent-post.php');
require_once ( get_template_directory() .'/include/widget-footer-contact.php');
require_once ( get_template_directory() .'/include/widget-footer-newsletter.php');


/*-----------------------------------------------------------------------------------*/
/*	Add Widget Areas
/*-----------------------------------------------------------------------------------*/
if (function_exists('register_sidebar')) {

        // Default Sidebar
        register_sidebar(array(
            'id' => 'blog-post',
            'name' => esc_html__('Blog Post',  'medical-pro'),
            'description' => esc_html__('This sidebar is for blog posts.',  'medical-pro'),
            'before_widget' => '<div id="%1$s" class="row m0 widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget_heading">',
            'after_title' => '</h5>'
        ));

        // Service Detail Page Sidebar
        register_sidebar(array(
            'id' => 'service-detail-page',
            'name' => esc_html__('Service Detail Page', 'medical-pro'),
            'description' => esc_html__('This sidebar is for service detail page.', 'medical-pro'),
            'before_widget' => '<div id="%1$s" class="row m0 widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget_heading">',
            'after_title' => '</h5>'
        ));

        // Footer Widget 1
        register_sidebar(array(
            'id' => 'footer-widget-1',
            'name' => esc_html__('Footer Widget 1', 'medical-pro'),
            'description' => esc_html__('This widget is for footer.', 'medical-pro'),
            'before_widget' => '<div id="%1$s" class="row m0 widget  %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="heading row m0"><h3>',
            'after_title' => '</h3></div>'
        ));

        // Footer Widget 2
        register_sidebar(array(
            'id' => 'footer-widget-2',
            'name' => esc_html__('Footer Widget 2', 'medical-pro'),
            'description' => esc_html__('This widget is for footer.', 'medical-pro'),
            'before_widget' => '<div id="%1$s" class="row m0 widget  %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="heading row m0"><h3>',
            'after_title' => '</h3></div>'
        ));

        // Footer Widget 3
        register_sidebar(array(
            'id' => 'footer-widget-3',
            'name' => esc_html__('Footer Widget 3', 'medical-pro'),
            'description' => esc_html__('This widget is for footer.', 'medical-pro'),
            'before_widget' => '<div id="%1$s" class="row m0 widget  %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="heading row m0"><h3>',
            'after_title' => '</h3></div>'
        ));
    
    


}
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

add_action( 'init', 'create_post_type' );



function create_post_type() {
    
  register_post_type( 'case_study',
    array(
      'labels' => array(
        'name' => __( 'Case Studies' ),
        'singular_name' => __( 'Case Study' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'custom-fields','thumbnail' )
    )
  );
    
    
    
register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'front events' ),
        'singular_name' => __( 'front event' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'custom-fields','thumbnail' )
    )
  );
    
register_post_type( 'testimonial',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'Testimonial' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'custom-fields','thumbnail' )
    )
  );


    register_post_type( 'blog',
        array(
            'labels' => array(
            'name' => __( 'Blog' ),
            'singular_name' => __( 'Blog' )
            ),
        'public' => true,
    'has_archive' => true,
    'supports' => array( 'title', 'editor', 'custom-fields','thumbnail' )
)
                    
);
flush_rewrite_rules();
}

// add_action( 'init', 'create_event_type' );
// function create_event_type() {
//   register_post_type( 'front_event',
//     array(
//       'labels' => array(
//         'name' => __( 'front events' ),
//         'singular_name' => __( 'front event' )
//       ),
//       'public' => true,
//       'has_archive' => true,
//       'supports' => array( 'title', 'editor', 'custom-fields')
//     )
//   );
// }

// add_action( 'init', 'create_post_type' );
// function create_post_type() {
//   register_post_type( 'front_events2',
//     array(
//       'labels' => array(
//         'name' => __( 'Front Events' ),
//         'singular_name' => __( 'Front Event' )
//       ),
//       'public' => true,
//       'has_archive' => true,
//       'supports' => array( 'title', 'editor', 'custom-fields' )
//     )
//   );
// }


add_filter('nav_menu_item_id', 'clear_nav_menu_item_id', 10, 3);
function clear_nav_menu_item_id($id, $item, $args) {
    return "";
}

add_filter('nav_menu_css_class', 'clear_nav_menu_item_class', 10, 3);
function clear_nav_menu_item_class($classes, $item, $args) {
    return array();
}


function new_excerpt_length( $length ) {
    return 20;
}


add_filter( 'excerpt_length', 'new_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}




add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
remove_filter('the_excerpt', 'wpautop');



