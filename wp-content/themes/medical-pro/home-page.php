<?php get_header(); ?>

<script src="<?php echo get_template_directory_uri() ?>/js/owl.carousel.min.js"></script>

<section>
    <div class="container">
        <div class="row">
            
            <div class="owl-carousel custom-owl">
                <div class="item"><h4>Media</h4></div>
                <div class="item"><h4>Upcoming Event</h4></div>
                <div class="item"><h4>Award</h4></div>
            </div>
            
            
        </div><!-- row -->
    </div><!-- container -->
</section><!-- crausal-->

<div>&nbsp;</div>


<section class="product padd-50">
    <div class="container">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/icon-1.png"></div>    
            <h2 class="titles">OUR product</h2>
                
                <ul class="ptab">
                    
                    <li><a href="#emergency" class="tab active">Emergency <span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/home/blue1.png"></span></a>
                    </li>
                    
                    <li><a href="#military" class="tab">Military <span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/home/blue2.png"></span></a>
                    </li>
                    
                    <li><a href="#vascular" class="tab">Vascular <span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/home/blue3.png"></span></a>
                    </li>
                    
                    <li class="last"><a href="#dental" class="tab">Dental <span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/home/blue4.png"></span></a>
                    </li> 
                    
                </ul>
              
                <div class="col-sm-12 tab-content active" id="emergency">
                    <div class="row">
                        
                        <div class="col-sm-5">
                           <img src="<?php echo get_template_directory_uri() ?>/images/home/emergency_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                           <h5>Emergency</h5>
                            <p class="italic">“Uncontrolled bleeding due to trauma injuries is the second leading cause of civilian deaths” </p>
                            <p>Axiostat® is a quick & effective intervention can stop most of these preventable deaths. Axiostat® has been proven to reduce the time taken to stop severe external bleeding in under 5 minutes.</p>
                            <a href="" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
            
                
                <div class="col-sm-12 tab-content" id="military">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="<?php echo get_template_directory_uri() ?>/images/home/emergency_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                            <h5>Military</h5>
                            <p class="italic">“One out of every two soldier deaths happens due to uncontrolled bleeding on the battlefield” </p>
                            <p>Axiostat® Military variant was designed to be used in battlefields and comes in camouflaged, rugged metal pouch packing for easy carrying</p>
                            
                            <p>Axiostat® is currently used by Defense Forces, Paramilitary Forces & Army across India, Middle East and Europe</p>
                            <a href="" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
            
            
                <div class="col-sm-12 tab-content" id="vascular">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="<?php echo get_template_directory_uri() ?>/images/home/emergency_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                            <h5>Vascular</h5>
                            <p class="italic">“Cardiac patients are often at high-risk of uncontrolled bleeding due to the blood thinners that are administered prior to catheterisation procedures.” </p>
                            <p>Not any more. Axiostat® Vascular dressing is proven to be a very effective and efficient means of stopping bleeding during Vascular procedures when compared to traditional compression methods that can take upto 30 minutes</p>
                            <a href="" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
            
            
                <div class="col-sm-12 tab-content" id="dental">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="<?php echo get_template_directory_uri() ?>/images/home/emergency_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                            <h5>Dental</h5>
                            <p class="italic">Often difficult to reach, dental bleeding can quickly become uncontrolled and unmanageable.</p>
                            <p>Our smallest offering designed to optimally fit into the bleeding cavity</p>
                            <a href="" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
        
        </div><!-- row -->
    </div><!-- container -->
</section><!-- product -->



<section class="casestudy padd-50">
    <div class="container">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/casestudy.png"></div>    
            <h2 class="titles">case study</h2>
        </div>
        
        
        <div class="row">
            <div class="col-sm-4">
                <img class="">
            </div><!-- col-->
            
            <div class="col-sm-4">
            
            </div><!-- col-->
            
            <div class="col-sm-4">
            
            </div><!-- col-->
        </div><!-- row -->
        
    </div>
</section>



<section class="blogs padd-50">
    <div class="container">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/blog.png"></div>    
            <h2 class="titles">Blogs</h2>
        </div>
        
        <div class="row">
            <div class="col-sm-4">
            
            </div><!-- col-->
            
            <div class="col-sm-4">
            
            </div><!-- col-->
            
            <div class="col-sm-4">
            
            </div><!-- col-->
        </div><!-- row -->
        
    </div>
</section> 



<section class="testimonials padd-50">
    <div class="container">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/testimonial.png"></div>    
            <h2 class="titles">testimonials</h2>
        </div>
        
        <div class="row">
            <div class="col-sm-4">
            
            </div><!-- col-->
            
            <div class="col-sm-4">
            
            </div><!-- col-->
            
            <div class="col-sm-4">
            
            </div><!-- col-->
        </div><!-- row -->
        
        
    </div>
</section>




<script>
jQuery(document).ready(function(){
    jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
});


</script>




<?php get_footer(); ?>