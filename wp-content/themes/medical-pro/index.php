<?php get_header(); ?>

<script src="<?php echo get_template_directory_uri() ?>/js/owl.carousel.min.js"></script>
<div>&nbsp;</div>
<div>&nbsp;</div>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <div class="owl-carousel custom-owl">
            <?php
            $args = array( 'post_type' => 'events', 'posts_per_page' => 6 );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
            ?>
                <div class="item" style="background: url(<?php echo the_post_thumbnail_url(); ?>) no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover;background-size: cover;">
                    <div class="inner-item">
                        <h4><?php the_title(); ?></h4>
                        <p><?php the_content(); ?></p>
                        <a href="<?php echo the_excerpt(); ?>" class="eextent np">Read More</a>
                    </div>
                </div>
            <?php endwhile; ?>
                
                
            </div>
         </div>   
            
        </div><!-- row -->
    </div><!-- container -->
</section><!-- crausal-->

<div>&nbsp;</div>
<div>&nbsp;</div>


<section class="product padd-50">
    <div class="container">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/icon-1.png"></div>    
            <h2 class="titles">PRODUCT RANGE</h2>
                
                <ul class="ptab">
                    <li><a href="#emergency" class="tab act">EMERGENCY HAEMOSTATIC DRESSING – M85, S55 <span class="eme"></span></a></li>
                    <li><a href="#military" class="tab">MILITARY HAEMOSTATIC DRESSING – M85, S55 <span class="mil"></span></a></li>
                    <li><a href="#vascular" class="tab">VASCULAR HAEMOSTATIC DRESSING-V55 <span class="vas"></span></a></li>
                    <li class="last"><a href="#dental" class="tab">DENTAL HAEMOSTATIC DRESSING – D11 <span class="den"></span></a></li> 
                </ul>
              
                <div class="col-sm-12 tab-content active" id="emergency">
                    <div class="row">
                        
                        <div class="col-sm-5">
                           <img src="<?php echo get_template_directory_uri() ?>/images/home/emergency_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                           <h5>Emergency HAEMOSTATIC DRESSING – M85, S55</h5>
                            <p class="italic">“Uncontrolled bleeding due to trauma injuries is the second leading cause of civilian deaths” </p>
                            <p>Axiostat® Emergency Haemostatic dressing is a quick & effective intervention can stop most of these preventable deaths. Axiostat® has been proven to reduce the time taken to stop severe external bleeding in under 5 minutes.</p>
                            <p>&nbsp;</p>
                            <a href="<?php echo site_url('/emergency/'); ?>" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
            
                
                <div class="col-sm-12 tab-content" id="military">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="<?php echo get_template_directory_uri() ?>/images/home/military_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                            <h5>Military HAEMOSTATIC DRESSING – MIL88, S55</h5>
                            <p class="italic">“One out of every two soldier deaths happens due to uncontrolled bleeding on the battlefield” </p>
                            <p>Axiostat® Military Haemostatic dressing designed to be used in battlefields and comes in camouflaged, rugged metal pouch packing for easy carrying</p>
                            
                            <p>Axiostat® is currently used by Defense Forces, Paramilitary Forces & Army across India, Middle East and Europe</p>
                            <p>&nbsp;</p>
                            <a href="<?php echo site_url('/military/'); ?>" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
            
            
                <div class="col-sm-12 tab-content" id="vascular">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="<?php echo get_template_directory_uri() ?>/images/home/vascular_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                            <h5>Vascular HAEMOSTATIC DRESSING-V55</h5>
                            <p class="italic">“Cardiac patients are often at high-risk of uncontrolled bleeding due to the blood thinners that are administered prior to catheterisation procedures.” </p>
                            <p>Not any more. Axiostat® Vascular Haemostatic dressing is proven to be a very effective and efficient means of stopping bleeding during Vascular procedures when compared to traditional compression methods that can take upto 30 minutes</p>
                            <p>&nbsp;</p>
                            <a href="<?php echo site_url('/vascular/'); ?>" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
            
            
                <div class="col-sm-12 tab-content" id="dental">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="<?php echo get_template_directory_uri() ?>/images/home/dental_thumb.jpg" class="img-responsive" >
                        </div>
                        
                        <div class="col-sm-7">
                            <h5>Dental HAEMOSTATIC DRESSING – D11</h5>
                            <p class="italic">Axiostat® Dental Haemostatic dressing, specially designed to control severe bleeding during dental procedures </p>
                            <p>Often difficult to reach, dental bleeding can quickly become uncontrolled and unmanageable.</p>
                            <p>Our smallest offering designed to optimally fit into the bleeding cavity</p>
                            <p>&nbsp;</p>
                            <a href="<?php echo site_url('/dental/'); ?>" class="rmore">Read More</a>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm- 12-->
        
        </div><!-- row -->
    </div><!-- container -->
</section><!-- product -->



<section class="blogs padd-50">
    <div class="container">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/blog.png"></div>    
            <h2 class="titles">Blogs</h2>
        </div>
        
         <div class="row">
          
            <?php
            
            $args = array( 'post_type' => 'blog', 'posts_per_page' => 4 );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
            ?>
          
            <div class="col-sm-3 blog-case">
                <a href="<?php the_permalink(); ?>">
                    <?php echo the_post_thumbnail(); ?>
                    <h3><?php the_title(); ?></h3>
                    <?php the_excerpt(); ?>
                </a>
    
            </div><!-- col-->
        <?php endwhile; ?>
        <div class="clearfix">&nbsp;</div>
       <div class="ee">
        <a href="<?php echo site_url('/blog/'); ?>" class="rmore">Read More</a>
       </div> 
             
        </div><!-- row -->
        
    </div>
</section> 



<section class="testimonials padd-50">
    <div class="container2">
        <div class="row">
            <div class="heading-img"><img src="<?php echo get_template_directory_uri(); ?>/images/home/testimonial.png"></div>    
            <h2 class="titles">testimonials</h2>
        </div>
        
        
        <div id="owl-demo" class="owl-carousel owl-theme">
              
        <?php
            
            $args = array( 'post_type' => 'testimonial', 'posts_per_page' => 15 );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
        ?>
          
            <div class="item">
            
                <div class="col-sm-12 head">
                    <div class="row">
                        <div class="col-sm-2"><?php echo the_post_thumbnail(); ?></div><!-- col-->
                        <div class="col-sm-10 cname"><?php echo the_title(); ?><div class="cnames"><?php echo get_post_meta(get_the_ID(),'doctor_speciality',true); ?></div></div><!-- col-->
                    </div>
                </div>
           
                <div class="col-sm-12 detail"><?php the_content(); ?></div>
          
            </div>
            <?php endwhile; ?>
        </div>  
      </div>
</section>




<script>
jQuery(document).ready(function(){
    jQuery('.custom-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
    
  jQuery('#owl-demo').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
    
    
jQuery("ul.ptab a").hover(function(event){
        
    event.preventDefault();
    jQuery(".tab").removeClass("act");
    jQuery(this).addClass("act");
    var datas=jQuery(this).attr("href");
    jQuery(".tab-content").removeClass("active");
    jQuery(datas).addClass("active");
});
    
jQuery(".case a").click(function(){
    var dat=jQuery(this).attr("data-link");
    jQuery("#yout").attr('src',dat);
});
    
});


</script>


<!-- Modal -->
<div id="youTube" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Case Study</h4>
      </div>
      <div class="modal-body">
        <iframe width="100%" border="0" height="315" src="" id="yout"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<?php get_footer(); ?>