<?php get_header(); ?>

    <section class="row bgf">
        <div class="container">

            <?php
            if (have_posts()) :

                // Start the loop.
                while (have_posts()) : the_post();

                    ?>
                    <section class="row breadcrumbRow">
                        <div class="inner m0">
                            <?php echo medical_pro_breadcrumb(); ?>
                        </div>
                    </section>
                    <section class="row">
                         <div class="entry-content"><?php the_content(); ?></div>
                    </section>
                <?php
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
                    // End the loop.
                endwhile;
            else :
                get_template_part('content', 'none');
            endif;
            ?>
        </div>
    </section>

<?php get_footer(); ?>