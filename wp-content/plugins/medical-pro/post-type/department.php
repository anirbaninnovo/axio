<?php

/* Create the testimonial custom post type */

if(!function_exists('medical_pro_create_department_post_type'))
{
    function medical_pro_create_department_post_type()
    {
        $labels = array(
            'name'          => esc_html__('Departments','medical-pro'),
            'singular_name' => esc_html__('Department','medical-pro'),
            'add_new'       => esc_html__('Add New','medical-pro'),
            'add_new_item'  => esc_html__('Add New Department','medical-pro'), //The add new item text. Default is Add New Post/Add New Page
            'edit_item'     => esc_html__('Edit Department','medical-pro'), //The edit item text. In the UI, this label is used as the main header on the post's editing panel. The default is "Edit Post" for non-hierarchical and "Edit Page" for hierarchical post types.
            'new_item'      => esc_html__('New Department','medical-pro'), //The new item text. Default is "New Post" for non-hierarchical and "New Page" for hierarchical post types.
            'view_item'     => esc_html__('View Department','medical-pro'), //The view item text. Default is View Post/View Page
            'search_items'  => esc_html__('Search Department','medical-pro'), //The search items text. Default is Search Posts/Search Pages
            'not_found'     =>  esc_html__('No department found','medical-pro'), //The not found text. Default is No posts found/No pages found
            'not_found_in_trash' => esc_html__('No department found in Trash','medical-pro'), //The not found in trash text. Default is No posts found in Trash/No pages found in Trash.
            'parent_item_colon' => '' //The parent text. This string is used only in hierarchical post types. Default is "Parent Page".
        );

        $args = array(
            'labels'        => $labels, //labels - An array of labels for this post type. By default, post labels are used for non-hierarchical post types and page labels for hierarchical ones.
            'public'        => true, //Controls how the type is visible to authors (show_in_nav_menus, show_ui) and readers
            'hierarchical'  => false, //Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
            'menu_position' => 5, //The position in the menu order the post type should appear. show_in_menu must be true.
            'supports'      => array('title','editor','thumbnail'), // Possible attributes (title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats)
            'rewrite'       => array('slug' => 'department'), // Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
            'exclude_from_search' => true, //Whether to exclude posts with this post type from front end search results.
            'publicly_queryable' => true //Whether queries can be performed on the front end as part of parse_request().
        );

        register_post_type('department', $args);
    }
}
add_action('init', 'medical_pro_create_department_post_type');
