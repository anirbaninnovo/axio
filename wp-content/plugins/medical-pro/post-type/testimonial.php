<?php

/* Create the testimonial custom post type */

if(!function_exists('medical_pro_create_testimonial_post_type'))
{
    function medical_pro_create_testimonial_post_type()
    {
        $labels = array(
            'name'          => esc_html__('Testimonials','medical-pro'),
            'singular_name' => esc_html__('Testimonial','medical-pro'),
            'add_new'       => esc_html__('Add New','medical-pro'),
            'add_new_item'  => esc_html__('Add New Testimonial','medical-pro'), //The add new item text. Default is Add New Post/Add New Page
            'edit_item'     => esc_html__('Edit Testimonial','medical-pro'), //The edit item text. In the UI, this label is used as the main header on the post's editing panel. The default is "Edit Post" for non-hierarchical and "Edit Page" for hierarchical post types.
            'new_item'      => esc_html__('New Testimonial','medical-pro'), //The new item text. Default is "New Post" for non-hierarchical and "New Page" for hierarchical post types.
            'view_item'     => esc_html__('View Testimonial','medical-pro'), //The view item text. Default is View Post/View Page
            'search_items'  => esc_html__('Search Testimonial','medical-pro'), //The search items text. Default is Search Posts/Search Pages
            'not_found'     =>  esc_html__('No testimonial found','medical-pro'), //The not found text. Default is No posts found/No pages found
            'not_found_in_trash' => esc_html__('No testimonial found in Trash','medical-pro'), //The not found in trash text. Default is No posts found in Trash/No pages found in Trash.
            'parent_item_colon' => '' //The parent text. This string is used only in hierarchical post types. Default is "Parent Page".
        );

        $args = array(
            'labels'        => $labels, //labels - An array of labels for this post type. By default, post labels are used for non-hierarchical post types and page labels for hierarchical ones.
            'public'        => false, //Controls how the type is visible to authors (show_in_nav_menus, show_ui) and readers
            'show_ui'       => true,
            'show_in_menu'  => true,
            'hierarchical'  => false, //Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
            'menu_position' => 5, //The position in the menu order the post type should appear. show_in_menu must be true.
            'supports'      => array('title','editor','thumbnail'), // Possible attributes (title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats)
            'rewrite'       => array('slug' => 'testimonial'), // Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
            'exclude_from_search' => true, //Whether to exclude posts with this post type from front end search results.
            'publicly_queryable' => false //Whether queries can be performed on the front end as part of parse_request().
        );

        register_post_type('testimonial', $args);
    }
}
add_action('init', 'medical_pro_create_testimonial_post_type');


/* Manage testimonial custom columns */
if (!function_exists('medical_pro_testimonial_edit_columns')) {
    function medical_pro_testimonial_edit_columns()
    {
        $columns = array(
            "cb" => '<input type="checkbox" >',
            "title" => esc_html__('Title', 'medical-pro'),
            "thumbnail" => esc_html__('Thumbnail', 'medical-pro'),
            "url" => esc_html__('URL', 'medical-pro'),
            "date" => esc_html__('Date', 'medical-pro')
        );

        return $columns;
    }
}
add_filter("manage_edit-testimonial_columns", "medical_pro_testimonial_edit_columns");


/* Manage testimonial display custom columns */
if (!function_exists('medical_pro_testimonial_custom_columns')) {
    function medical_pro_testimonial_custom_columns($column){
        global $post;
        switch ($column)
        {
            case 'url' :
                $url = get_post_meta($post->ID, 'website_url', true);
                if(!empty($url))
                {
                    echo esc_url($url);
                } else {
                    esc_html_e('N/A', 'medical-pro');
                }
                break;
        }
    }
}
add_action("manage_posts_custom_column", "medical_pro_testimonial_custom_columns");