<?php
/*
Plugin Name: Medical Pro
Plugin URI: http://medicalpro.themedesigner.in
Description: Medical Pro Theme support
Author: Themedesigner
Version: 1.0.0
Author URI: http://www.themedesigner.in
Text Domain: medical-pro
*/

$plugin_dir_path = plugin_dir_path( __FILE__ );

if(!function_exists('medical_pro_load_wp_admin_textdomain'))
{
    function medical_pro_load_wp_admin_textdomain() {

        load_plugin_textdomain('medical-pro', false, dirname( plugin_basename(__FILE__) ) . '/languages/');

    }
}
add_action('plugins_loaded', 'medical_pro_load_wp_admin_textdomain');


/*-----------------------------------------------------------------------------------*/
/*	Get available header style list
/*-----------------------------------------------------------------------------------*/
if(!function_exists('medical_pro_get_header_styles'))
{
    function medical_pro_get_header_styles()
    {
        return array(
            '1' => esc_html__('Header Style 1', 'medical-pro'),
            '2' => esc_html__('Header Style 2', 'medical-pro'),
			'3' => esc_html__('Header Style 3', 'medical-pro'),
        );
    }
}

//Get revolution slider drop down options
if(!function_exists('medical_pro_get_rev_slider_dropdown'))
{
    function medical_pro_get_rev_slider_dropdown()
    {
        $sliders = array();

        if (class_exists('RevSliderAdmin')) {
            $slider = new RevSlider();
            $arrSliders = $slider->getArrSlidersShort();
            if(!empty($arrSliders))
            {
                foreach($arrSliders as $id => $title)
                {
                    $slider = new RevSlider();
                    $slider->initByID($id);

                    $sliders[$slider->getAlias()] = $title;
                }
            }
        }
        return $sliders;
    }
}

/*-----------------------------------------------------------------------------------*/
/*	MedicalPro the excerpt
/*-----------------------------------------------------------------------------------*/
if (!function_exists('medical_pro_excerpt'))
{
    function medical_pro_excerpt($length = 3000){
        return substr(get_the_excerpt(),0,$length);
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Function to output different bootstrap classes
/*-----------------------------------------------------------------------------------*/
if (!function_exists('medical_pro_bc'))
{
    function medical_pro_bc($col_md = null,$col_sm = null)
    {
        $bootstrap_grid_class = "";
        if (!empty($col_md)) {
            $bootstrap_grid_class .= "col-md-$col_md ";
        }
        if (!empty($col_sm)) {
            $bootstrap_grid_class .= "col-sm-$col_sm ";
        }
        return $bootstrap_grid_class;
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Include Custom Post Types
/*-----------------------------------------------------------------------------------*/
require_once ( $plugin_dir_path . 'post-type/doctor.php');
require_once ( $plugin_dir_path . 'post-type/department.php');
require_once ( $plugin_dir_path . 'post-type/service.php');
require_once ( $plugin_dir_path . 'post-type/testimonial.php');


/*-----------------------------------------------------------------------------------*/
/*	Include VC Extends and Short Code
/*-----------------------------------------------------------------------------------*/
require_once ( $plugin_dir_path . 'vc_extend/services.php');
require_once ( $plugin_dir_path . 'vc_extend/testimonials.php');
require_once ( $plugin_dir_path . 'vc_extend/recent_posts.php');
require_once ( $plugin_dir_path . 'vc_extend/doctors.php');
require_once ( $plugin_dir_path . 'vc_extend/department.php');
require_once ( $plugin_dir_path . 'vc_extend/appointment_form.php');
require_once ( $plugin_dir_path . 'vc_extend/appointment_horizontal_form.php');


/*-----------------------------------------------------------------------------------*/
/*	Include Meta boxes config
/*-----------------------------------------------------------------------------------*/
require_once ( $plugin_dir_path . 'metaboxes.php');