<?php

/* Create header options meta boxes */
if(!function_exists('medical_pro_header_options_metaboxes'))
{
    function medical_pro_header_options_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'header_options',
            'title'         => esc_html__('Header Options', 'medical-pro'),
            'object_types'  => array('page', 'post','doctor','service'), // Post type
            'context'       => 'side',
            'priority'      => 'low',
            'show_names'    => false, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Select Header', 'medical-pro'),
            'id'    => 'header_style',
            'type'  => 'select',
            'options' => array(0 => esc_html__('Select header style', 'medical-pro')) + medical_pro_get_header_styles()
        ));



        $sliders = array('0' => 'Select Revolution Slider') + medical_pro_get_rev_slider_dropdown();

        $cmb->add_field(array(
            'name'  => esc_html__('Select Slider', 'medical-pro'),
            'id'    => 'slider_alias',
            'type'  => 'select',
            'options'  => $sliders,
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Image', 'medical-pro'),
            'id'    => 'header_image',
            'type'  => 'file',
            'desc'  => 'This field will be ignored,  if you are selected revolution slider.',
            'options' => array(
                'url' => false, // Hide the text input for the url
                'add_upload_file_text' => 'Select Image' // Change upload button text. Default: "Add or Upload File"
            )
        ));


        $cmb->add_field(array(
            'name'  => esc_html__('Title', 'medical-pro'),
            'id'    => 'title',
            'type'  => 'text',
            'desc'  => 'Enter title, if you are select image',
            'inline'  => true,
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Sub Title', 'medical-pro'),
            'id'    => 'sub_title',
            'type'  => 'text',
            'desc'  => 'Enter sub title, if you are select image'
        ));


    }
}

add_action('cmb2_init', 'medical_pro_header_options_metaboxes');


/* Create doctor meta boxes */
if(!function_exists('medical_pro_doctor_metaboxes'))
{
    function medical_pro_doctor_metaboxes()
    {
        $prefix = 'doctor_';

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'doctor_information',
            'title'         => esc_html__('Doctor Information', 'medical-pro'),
            'object_types'  => array('doctor'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Introduction Text', 'medical-pro'),
            'id'    => $prefix . 'introduction_text',
            'type'  => 'textarea_small',
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Speciality', 'medical-pro'),
            'id'    => $prefix . 'speciality',
            'type'  => 'text',
            'repeatable' => true
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Address', 'medical-pro'),
            'id'    => $prefix . 'address',
            'type'  => 'textarea_small'
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Educations', 'medical-pro'),
            'id'    => $prefix . 'educations',
            'type'  => 'textarea_small'
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Educations', 'medical-pro'),
            'id'    => $prefix . 'educations',
            'type'  => 'textarea_small'
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Awards & Recognition', 'medical-pro'),
            'id'    => $prefix . 'awards_n_recognition',
            'type'  => 'textarea_small'
        ));

        $group_field_id = $cmb->add_field( array(
            'id'          => 'office_hours',
            'type'        => 'group',
            'options'     => array(
                'group_title'   => esc_html__( 'Office work day and time', 'medical-pro' ). ' {#}', // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Entry', 'medical-pro' ),
                'remove_button' => esc_html__( 'Remove Entry', 'medical-pro' ),
                //'sortable'      => true, // beta
            ),
        ));

        $cmb->add_group_field($group_field_id, array(
            'name'  => esc_html__('Work Day', 'medical-pro'),
            'id'    => $prefix . 'office_work_day',
            'type'  => 'text',
            'desc'  => 'Example: Monday, Tuesday and Thursday'
        ));


        $cmb->add_group_field($group_field_id, array(
            'name'  => esc_html__('Work Time', 'medical-pro'),
            'id'    => $prefix . 'office_work_time',
            'type'  => 'text',
            'desc'  => 'Example: 8am - 5pm'
            // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Certification(s)', 'medical-pro'),
            'id'    => $prefix . 'certification',
            'type'  => 'textarea_small'
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Contact Info', 'medical-pro'),
            'id'    => $prefix . 'contact_info',
            'type'  => 'textarea_small'
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Facebook URL', 'medical-pro'),
            'id'    => $prefix . 'facebook_url',
            'type'  => 'text_url',
            'protocols' => array('http', 'https')
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Twitter URL', 'medical-pro'),
            'id'    => $prefix . 'twitter_url',
            'type'  => 'text_url',
            'protocols' => array('http', 'https')
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Google Plus URL', 'medical-pro'),
            'id'    => $prefix . 'google_plus_url',
            'type'  => 'text_url',
            'protocols' => array('http', 'https')
        ));

        $cmb = new_cmb2_box(array(
            'id'            => 'banner_settings',
            'title'         => esc_html__('Banner Image', 'medical-pro'),
            'object_types'  => array('doctor'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => false, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Banner Image', 'medical-pro'),
            'id'    => $prefix . 'banner_img',
            'type'  => 'file',
            'options' => array('url' => true),
            'desc' => 'Banner image should have minimum width of 2000px and minimum height of 220px.'
        ));

        $cmb = new_cmb2_box(array(
            'id'            => 'doctor_departments',
            'title'         => esc_html__('Department', 'medical-pro'),
            'object_types'  => array('doctor'), // Post type
            'context'       => 'side',
            'priority'      => 'high',
            'show_names'    => false, // Show field names on the left
        ));

        $department_options = array();
        $departments = query_posts(array('post_type' => 'department'));
        if(!empty($departments))
        {
            foreach($departments as $departments)
            {
                $department_options[$departments->ID] = $departments->post_title;
            }
        }
        wp_reset_query();

        $cmb->add_field( array(
            'id'          => 'department_id',
            'type'        => 'multicheck',
            'options'     => $department_options
        ));
    }
}

add_action('cmb2_init', 'medical_pro_doctor_metaboxes');


/* Create testimonial meta boxes */
if(!function_exists('medical_pro_testimonial_metaboxes'))
{
    function medical_pro_testimonial_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'testimonial',
            'title'         => esc_html__('URL', 'medical-pro'),
            'object_types'  => array('testimonial'), // Post type
            'context'       => 'side',
            'priority'      => 'low',
            'show_names'    => false, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('URL', 'medical-pro'),
            'id'    => 'website_url',
            'type'  => 'text'
        ));
    }
}

add_action('cmb2_init', 'medical_pro_testimonial_metaboxes');


/* Create Services meta boxes */
if(!function_exists('medical_pro_services_metaboxes'))
{
    function medical_pro_services_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'service_information',
            'title'         => esc_html__('Service Information', 'medical-pro'),
            'object_types'  => array('service'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

		$cmb->add_field(array(
            'name'  => esc_html__('Service Subtitle', 'medical-pro'),
			'desc' 	=> esc_html__('Provide subtitle text for style 2', 'medical-pro'),
            'id'    => 'serivce_subtitle',
            'type'  => 'textarea_small'
        ));
		
		$cmb->add_field(array(
            'name'  => esc_html__('Service ICON', 'medical-pro'),
			'desc' 	=> esc_html__('Upload service icon image.(Size : 36 x 33)', 'medical-pro'),
            'id'    => 'serivce_icon',
            'type'  => 'file'
        ));
		
		$cmb->add_field(array(
            'name'  => esc_html__('Service ICON Hover', 'medical-pro'),
			'desc' 	=> esc_html__('Upload service icon image.(Size : 36 x 33)', 'medical-pro'),
            'id'    => 'serivce_icon_hover',
            'type'  => 'file'
        ));
		$cmb->add_field(array(
            'name'  => esc_html__('Service ICON for detail page', 'medical-pro'),
			'desc' 	=> esc_html__('Upload service icon image for details page and Style 2 option.(Size : 48 x 42)', 'medical-pro'),
            'id'    => 'serivce_icon_style2',
            'type'  => 'file'
        ));
    }
}

add_action('cmb2_init', 'medical_pro_services_metaboxes');


/* Create Services meta boxes */
if(!function_exists('medical_pro_department_metaboxes'))
{
    function medical_pro_department_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'department_information',
            'title'         => esc_html__('Department Information', 'medical-pro'),
            'object_types'  => array('department'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

		$cmb->add_field(array(
            'name'  => esc_html__('Department Subtitle', 'medical-pro'),
			'desc' 	=> esc_html__('Provide subtitle text for style 2', 'medical-pro'),
            'id'    => 'department_subtitle',
            'type'  => 'textarea_small'
        ));
    }
}

add_action('cmb2_init', 'medical_pro_department_metaboxes');


/* Create Post meta boxes for Video post Type*/
if(!function_exists('medical_pro_video_metaboxes'))
{
    function medical_pro_video_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'video_meta_box',
            'title'         => esc_html__('Video Information', 'medical-pro'),
            'object_types'  => array('post'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Video Embed Code', 'medical-pro'),
            'id'    => 'video_embed_code',
            'type'  => 'textarea_small'
        ));
    }
}

add_action('cmb2_init', 'medical_pro_video_metaboxes');


/* Create Post meta boxes for Audio post Type*/
if(!function_exists('medical_pro_audio_metaboxes'))
{
    function medical_pro_audio_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'audio_meta_box',
            'title'         => esc_html__('Audio Information', 'medical-pro'),
            'object_types'  => array('post'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('MP3 Audio File', 'medical-pro'),
			'desc' 	=> esc_html__('Upload the MP3 audio file.', 'medical-pro'),
            'id'    => 'mp3_audio_file',
            'type'  => 'file'
        ));
		
		$cmb->add_field(array(
            'name'  => esc_html__('Audio Embed Code', 'medical-pro'),
			'desc' => esc_html__('If you do not have audio files to upload, then you can provide audio embed code here.', 'medical-pro'),
            'id'    => 'audio_embed_code',
            'type'  => 'textarea_small'
        ));
    }
}

add_action('cmb2_init', 'medical_pro_audio_metaboxes');


/* Create Post meta boxes for Link post Type*/
if(!function_exists('medical_pro_link_metaboxes'))
{
    function medical_pro_link_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'link_meta_box',
            'title'         => esc_html__('Link Information', 'medical-pro'),
            'object_types'  => array('post'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Link URL', 'medical-pro'),
            'id'    => 'link_url',
            'type'  => 'text'
        ));
    }
}
add_action('cmb2_init', 'medical_pro_link_metaboxes');


/* Create Post meta boxes for Gallay post Type*/
if(!function_exists('medical_pro_gallary_metaboxes'))
{
    function medical_pro_gallary_metaboxes()
    {
        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'gallery_meta_box',
            'title'         => esc_html__('Gallery Information', 'medical-pro'),
            'object_types'  => array('post'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Gallery Images', 'medical-pro'),
			'desc' => esc_html__('An image should have minimum width of 732px and minimum height of 447px ( Bigger size images will be cropped automatically).', 'medical-pro'),
            'id'    => 'gallery',
            'type'  => 'file_list'
        ));

    }
}
add_action('cmb2_init', 'medical_pro_gallary_metaboxes');

/* Create Post meta boxes for Quote post Type*/
if(!function_exists('medical_pro_quote_metaboxes'))
{
    function medical_pro_quote_metaboxes()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'            => 'quote_meta_box',
            'title'         => esc_html__('Quote Information', 'medical-pro'),
            'object_types'  => array('post'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'  => esc_html__('Quote Text', 'medical-pro'),
            'id'    => 'quote_text',
            'type'  => 'textarea_small'
        ));
		
		 $cmb->add_field(array(
            'name'  => esc_html__('Quote Author', 'medical-pro'),
            'id'    => 'quote_author',
            'type'  => 'text'
        ));
    }
}
add_action('cmb2_init', 'medical_pro_quote_metaboxes');



