<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'innovode_axio');

/** MySQL database username */
define('DB_USER', 'innovode_axio');

/** MySQL database password */
define('DB_PASSWORD', 'axio123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l+lxToUTT,!R+k:|EIEA(ym]-+?iU+DXfF48J83U!uRexJpVmj5~Ya_912iR$)$o');
define('SECURE_AUTH_KEY',  'lZn.cY!/+Na?+ER%4 ur/n9V6;f/Y/IG?rVb&-;n0.$seLX]}=y?(P| jec-=vD%');
define('LOGGED_IN_KEY',    '<~Eyw{JP&iX@stY.6z-Hdqx{WD7C~vRMeBv!oS3#bh+Lo[>|84e5ztXusgk-z?N.');
define('NONCE_KEY',        'F&DNS=U|X%]wPrd@4JD>35H6QY7isbb +D_jWcOSt(3SW?FDEOhzV]NoA)^{VWzO');
define('AUTH_SALT',        '-f*Ujtzt6IU_~Dx-pXL&+.h^wJN.+KG-6kMN-0rJ<-U5cPqGXt}jw*w|bQ=6LJQH');
define('SECURE_AUTH_SALT', 'K4_+}k|9.g8L*[^#8L^6 hbGVGkgX pI0i G-X7;$ +; [X}OAj`#2MpaZMz<{b-');
define('LOGGED_IN_SALT',   ' i!~mc5=EH-&AMBDUJQr{xFH.aRCD{WrV>J2q-:z|K#};|`B*c%1)A</c?.[t-r9');
define('NONCE_SALT',       '~;?oqK%#G4Gy2sIFc4iJ-vbax|9+(01{0Sk@!uK0qeV?SbstDY<ZOng{]]V*/W!}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
